section .text
; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor rax, rax
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
	xor rax, rax
	push rdi
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	mov rax, 1
	syscall
	pop rdi
	ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	xor rcx, rcx
	dec rsp
	mov [rsp], al
	mov r10, 10
	mov rax, rdi
.iter:
	xor rdx, rdx
	div r10
	add dl, 0x30
	dec rsp
	mov [rsp], dl
	inc rcx
	test rax, rax
	jnz .iter
.print:
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
	inc rsp
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	xor rax, rax
	cmp rdi, 0
	jge .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
.print:
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	xor r11, r11
	xor r10, r10
	xor r9, r9
	call string_length
	mov r11, rax
	push rdi
	mov rdi, rsi
	call string_length
	pop rdi
	cmp rax, r11
	je .iter
	jmp .neq
.iter:
	lea r10, [rdi + rax]
	lea r9, [rsi + rax]
	mov r10b, byte[r10]
	cmp r10b, byte[r9]
	jne .neq
	cmp byte[r9], 0
	je .eq
	inc rax
	jmp .iter
.eq:
	mov rax, 1
	ret
.neq:
	mov rax, 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push rax
	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rax, rax
	xor rdx, rdx
	xor r8, r8
.blank:
	push rdi
	push rdx
	push rsi
	call read_char
	pop rsi
	pop rdx
	pop rdi
	cmp al, 0x20
	je .blank
	cmp al, 0x9
	je .blank
	cmp al, 0xA
	je .blank
.loop:
	cmp rsi, rdx
	jle .fail
	lea r8, [rdi + rdx]
	mov [r8], al
	cmp al, 0
	je .success
	inc rdx
	push rdi
	push rdx
	push rsi
	call read_char
	pop rsi
	pop rdx
	pop rdi
	cmp al, 0x20
	je .success
	cmp al, 0x9
	je .success
	cmp al, 0xA
	je .success
	jmp .loop
.fail:
	mov rax, 0
	ret
.success:
	mov byte [rdi + rdx], 0
	mov rax, rdi
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	mov r9, 10
.iter:
	cmp byte [rdi], '0'
	jb .fin
	cmp byte [rdi], '9'
	ja .fin
	push rdx
	mul r9
	pop rdx
	add al, byte [rdi]
	sub al, 0x30
	inc rdx
	inc rdi
	jmp .iter
.fin:
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	xor rdx, rdx
	xor r11, r11
	xor r10, r10
.sign:
	cmp byte [rdi], '-'
	je .min
	jmp .num
.min:
	inc r10
	inc rdi
.num:
	push rdx
	call parse_uint
	pop r11
	cmp r10, 1
	jne .fin
	inc rdx
	neg rax
.fin:
	add rdx, r11
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor r10, r10
.copy:
	call string_length
	inc rax
    cmp rax, rdx
    ja .no
	mov rcx, rax
	push rdi
	mov rdi, rsi
	pop rsi
	rep movsb
	jmp .res
.no:
	mov rax, 0
    ret
.res:
	mov rax, r10
	ret
